import 'dart:io';
// import 'package:midterm/midterm.dart' as midterm;

List<String> listInfix = [];
List<String> listPostfix = [];
List<String> listOperator = [];
List<int> result = [];

void main() {
//input
  print("input Infix : ");
  var word = stdin.readLineSync()!;

  infix(word);
  print(": Infix :");
  print(listInfix);
  infixToPost(listInfix);
  print(": Postfix :");
  print(listPostfix);
  cal(listPostfix);
  print(": Result :");
  print(result);
}

//infix
List<String> infix(var x) {
  listInfix = x.split(" ");
  return listInfix;
}

//postfix
void infixToPost(List<String> op) {
  for (var i = 0; i < listInfix.length; i++) {
    if (op[i] == "+" ||
        op[i] == "-" ||
        op[i] == "*" ||
        op[i] == "/" ||
        op[i] == "^") {
      while (listOperator.isNotEmpty &&
          !(listOperator.last == "(") &&
          (op[i] == "+" ||
              op[i] == "-" && listOperator.last == "*" ||
              listOperator.last == "/" ||
              listOperator.last == "^")) {
        listPostfix.add(listOperator.last);
        listOperator.removeLast();
      }
      while (listOperator.isNotEmpty &&
          !(listOperator.last == "(") &&
          (op[i] == "*" ||
              op[i] == "/" && listOperator.last == "*" ||
              listOperator.last == "/" ||
              listOperator.last == "^")) {
        listPostfix.add(listOperator.last);
        listOperator.removeLast();
      }
      listOperator.add(op[i]);
    } else if (op[i] == "(") {
      listOperator.add(op[i]);
    } else if (op[i] == ")") {
      while (!(listOperator.last == "(")) {
        listPostfix.add(listOperator.last);
        listOperator.removeLast();
      }
      listOperator.removeLast();
    } else {
      listPostfix.add(op[i]);
    }
  }
  while (listOperator.isNotEmpty) {
    listPostfix.add(listOperator.last);
    listOperator.removeLast();
  }
}

//cal
void cal(List<String> op) {
  for (var i = 0; i < listPostfix.length; i++) {
    if (op[i] == "+" ||
        op[i] == "-" ||
        op[i] == "*" ||
        op[i] == "/" ||
        op[i] == "^") {
      int right = result.removeLast();
      int left = result.removeLast();
      int sum = 1;
      switch (op[i]) {
        case "+":
          result.add(left + right);
          break;
        case "-":
          result.add(left - right);
          break;
        case "*":
          result.add(left * right);
          break;
        case "/":
          result.add(left ~/ right);
          break;
        case "^":
          for (int o = 0; o < right; o++) {
            sum *= left;
          }
          result.add(sum);
          break;
      }
    } else {
      result.add(int.parse(op[i]));
    }
  }
}
